import React from 'react';
import Head from 'next/head';
import { Row, Col, Card, Container } from 'react-bootstrap'

export default function Projects(){
	return(
		<React.Fragment>
			<Head>
				<title>Hollnsteiner Portfolio - Projects</title>
			</Head>
		<Container>
			<Row>
				<Col xs={12} md={4}>
					<Card className="projectCards">
						<Card.Body>
							<Card.Title>
								<h2>Budget Tracker App</h2>
							</Card.Title>
							<Card.Text>
								Budget Tracker is an exciting new app that allows you to keep track of your finances.
							</Card.Text>
							<a href='https://capstone-3-client-2.vercel.app/' target="blank">Budget Tracker App</a>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
		</React.Fragment>
	)
}